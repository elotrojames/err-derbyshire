import collections
import re

import errbot


class Derbyshire(errbot.BotPlugin):

    REGULAR_EXPRESSIONS = collections.OrderedDict([
        ('[Hh]ola', 'o/'),

        ('el ?jaqueado', 'aww <3'),

        ('bot', '¿Qué ondas? ¿Hay un bot por acá?'),

        (':\(', 'Te mando un abrazo :('),

        ('https?:\/\/', (
            '¡Qué interesante! Mandalo también a '
            'https://bunqueer.jaquerespeis.org')),
    ])


    def callback_message(self, message):
        reply = self._get_message_reply(message.body)
        if reply:
            self.send(message.frm, reply)


    def _get_message_reply(self, message):
        for regular_expression, reply in self.REGULAR_EXPRESSIONS.items():
            if re.search(regular_expression, message):
                return reply
