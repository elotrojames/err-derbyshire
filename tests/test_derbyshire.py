import os
import unittest
from unittest import mock

import testscenarios
from errbot.backends import test


import derbyshire


class DerbyshireBotCommandsTestCase(unittest.TestCase):


    def setUp(self):
        super().setUp()
        self.testbot = test.TestBot(
            extra_plugin_dir=os.path.join(os.path.dirname(__file__), '..'),
            extra_config={'CORE_PLUGINS': None})
        self.testbot.start()
        self.addCleanup(self.testbot.stop)


    def test_reply(self):
        self.testbot.push_message(':(')
        self.assertEqual(
            'Te mando un abrazo :(',
            self.testbot.pop_message())


class DerbyshireRegexTestCase(testscenarios.TestWithScenarios):

    scenarios = [
        ('hola', {
            'message': 'dummy hola dummy',
            'expected_reply': 'o/'}),
        ('Hola', {
            'message': 'dummy Hola dummy',
            'expected_reply': 'o/'}),

        ('el jaqueado', {
            'message': 'dummy el jaqueado dummy',
            'expected_reply': 'aww <3'}),
        ('eljaqueado', {
            'message': 'dummy eljaqueado dummy',
            'expected_reply': 'aww <3'}),

        ('bot', {
            'message': 'dummy bot dummy',
            'expected_reply': '¿Qué ondas? ¿Hay un bot por acá?'}),


        (':(', {
            'message': 'dummy :( dummy',
            'expected_reply': 'Te mando un abrazo :('}),


        ('http', {
            'message': 'dummy http://dummy dummy',
            'expected_reply': (
                '¡Qué interesante! Mandalo también a '
                'https://bunqueer.jaquerespeis.org')}),
        ('https', {
            'message': 'dummy https://dummy dummy',
            'expected_reply': (
                '¡Qué interesante! Mandalo también a '
                'https://bunqueer.jaquerespeis.org')}),
    ]


    def setUp(self):
        super().setUp()
        self.derbyshire = derbyshire.Derbyshire(mock.MagicMock())


    def test_get_message_reply(self):
        reply = self.derbyshire._get_message_reply(self.message)
        self.assertEqual(reply, self.expected_reply)
